# Prueba Frontend

Pequeña aplicación web para listar y buscar películas de estudio Ghibli.

He utilizado [vue](https://vuejs.org/) para construir la aplicación, junto con el framework [vuetify](https://vuetifyjs.com/)
para agilizar el diseño visual y funcional de la aplicación.

Si tuviese tiempo de continuar con este proyecto, agregaría lo siguiente:
- Opción para listar/buscar otros objetos del api como people, locations, species, vehicles.
- Vista detallada para objetos del api que muestre objetos asociados y link a la vista de detalle de esos objetos.
- Integración de *server side rendering*, tal vez utilizando [nuxtjs](https://nuxtjs.org/)
- Opción para integrar certificados ssl en la imágen de producción.

Quiero destacar que actualmente la mayor parte de la lógica de la aplicación se concentra en el archivo [Films.vue](src/views/Films.vue),
realmente me parece muy versátil el componente `v-data-table` de vuetify porque se encarga de gran parte de la lógica que necesitaba.

He puesto una [instancia en execución](http://capipo.com:8082).

## Ejecución del aplicación

### Ejecutar servidor

```sh
yarn install
yarn serve
```

### Pruebas unitarias

```sh
yarn test:unit
```

### Pruebas end to end

Previamente iniciar applicación con `yarn serve`

```sh
yarn cypress install
yarn cypress run
```

## Ejecutar contenedor docker de producción

```
docker run -d -p8080:80 registry.gitlab.com/capipo/prueba-frontend
```

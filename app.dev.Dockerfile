FROM node:14.15.4

ENV PATH="$HOME/.yarn/bin:$PATH"

EXPOSE 8000 8000

RUN apt update -y && apt install -y libgtk2.0-0 libgtk-3-0 libgbm-dev libnotify-dev libgconf-2-4 libnss3 libxss1 libasound2 libxtst6 xauth xvfb && apt clean -y

RUN yarn global add @vue/cli && yarn cache clean

FROM node:14.15.4 as build
COPY ./ /app
RUN cd /app && yarn install && yarn build


FROM nginx:1.19.6
COPY --from=build /app/dist /usr/share/nginx/html

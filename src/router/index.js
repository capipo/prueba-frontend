import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: { name: 'Films' }
  },
  {
    path: '/films',
    name: 'Films',
    component: () => import(/* webpackChunkName: "films" */ '../views/Films.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router

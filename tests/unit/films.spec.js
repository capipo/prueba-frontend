import { shallowMount } from '@vue/test-utils'
import Films from '@/views/Films.vue'
import Vue from 'vue'
import Vuetify from 'vuetify'

Vue.use(Vuetify)

describe('Films.vue', () => {
  it('renders title', () => {
    const wrapper = shallowMount(Films, {})
    expect(wrapper.text()).toMatch('Ghibli Films')
  });

  describe('fetch', () => {
    it('fills items', async () => {
      const wrapper = shallowMount(Films, {})
      expect(wrapper.vm.$data.items.length).toBe(0)
      await wrapper.vm.fetch()
      expect(wrapper.vm.$data.items.length).not.toBe(0)
    });
  });
})

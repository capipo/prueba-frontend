// https://docs.cypress.io/api/introduction/api.html

describe('films', () => {
  it('Visits the films url', () => {
    cy.visit('/films')
    cy.contains('.v-toolbar__title', 'Ghibli Films')
  })

  it('Filters films', () => {
    cy.visit('/films')
    cy.get('tbody').find('tr', { timeout: 5000 }).its('length').should('be.gte', 1)

    cy.get('input').click().type('Princess Mononoke')
    cy.get('tbody').find('tr').its('length').should('eq', 1)
  })
})
